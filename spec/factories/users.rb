FactoryGirl.define do
  factory :user do
    email 'user@example.com'
    password 'password'
  end

  factory :user_two, class: User do
    email 'user_two@example.com'
    password 'password'
  end

  factory :admin, class: User do
    email 'admin@example.com'
    password 'password'
    admin true
  end
end