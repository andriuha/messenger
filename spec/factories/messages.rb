FactoryGirl.define do
  factory :message do
    association :from, factory: :user
    association :to, factory: :user_two
    readed false
    body 'Hi user'
  end
end