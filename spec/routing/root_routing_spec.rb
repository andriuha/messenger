require 'rails_helper'

RSpec.describe 'route for root', type: :routing do
  it 'routes to the front#index' do
    expect(:get => '/').to route_to('front#index')
  end
end