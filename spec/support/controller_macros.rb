module ControllerMacros
  def login_admin
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      admin = User.create(email: 'adminus2@example.com', password: 'password', admin: true, locked: false)
      sign_in admin
    end
  end
end

module LoginRequestHelper
  def login_admin
    admin = User.create(email: 'adminus2@example.com', password: 'password', admin: true, locked: false)
    post user_session_path, params: { user: { email: admin.email, password: admin.password } }
  end
end