require 'rails_helper'

RSpec.describe "front/index.html.haml", type: :view do
  it 'displays welcome text' do
    render
    expect(rendered).to match /Welcome to the best app ever :\)/
  end
end
