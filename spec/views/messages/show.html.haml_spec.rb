require 'rails_helper'

RSpec.describe "messages/show", type: :view do
  before(:each) do
    @message = assign(:message, Message.create!(
      :from => create(:admin, email: 'admin@example.com'),
      :to => create(:user, email: 'user@example.com'),
      :readed => false,
      :body => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/admin@example.com/)
    expect(rendered).to match(/user@example.com/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
  end
end
