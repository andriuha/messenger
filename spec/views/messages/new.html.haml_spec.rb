require 'rails_helper'

RSpec.describe "messages/new", type: :view do
  before(:each) do
    assign(:message, build(:message))
  end

  it "renders new message form" do
    render

    assert_select "form[action=?][method=?]", messages_path, "post" do

      assert_select "select[name=?]", "message[from_id]"

      assert_select "select[name=?]", "message[to_id]"

      assert_select "input[name=?]", "message[readed]"

      assert_select "textarea[name=?]", "message[body]"
    end
  end
end
