require 'rails_helper'

RSpec.describe "messages/index", type: :view do
  before(:each) do
    from = create(:admin, email: 'admin@example.com')
    to = create(:user, email: 'user@example.com')

    assign(:messages, [
      Message.create!(
        :from => from,
        :to => to,
        :readed => false,
        :body => "MyText"
      ),
      Message.create!(
        :from => from,
        :to => to,
        :readed => false,
        :body => "MyText"
      )
    ])
  end

  it "renders a list of messages" do
    render
    assert_select "tr>td", :text => 'admin@example.com', :count => 2
    assert_select "tr>td", :text => 'user@example.com', :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
