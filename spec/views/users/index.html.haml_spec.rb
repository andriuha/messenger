require 'rails_helper'

RSpec.describe "users/index", type: :view do
  before(:each) do
    assign(:users, [create(:user), create(:admin)])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => false.to_s, :count => 3
    assert_select "tr>td", :text => false.to_s, :count => 3
  end
end
