require 'rails_helper'

RSpec.describe "users/new", type: :view do
  before(:each) do
    assign(:user, build(:user))
  end

  it "renders new user form" do
    render

    assert_select "form[action=?][method=?]", users_path, "post" do

      assert_select "input#user_admin[name=?]", "user[admin]"

      assert_select "input#user_locked[name=?]", "user[locked]"
    end
  end
end
