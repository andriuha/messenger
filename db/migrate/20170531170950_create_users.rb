class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.boolean :admin
      t.boolean :locked
      t.string :name
      t.string :sex
      t.date :birthday

      t.timestamps
    end
  end
end
