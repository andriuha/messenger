class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.references :from
      t.references :to
      t.boolean :readed
      t.text :body

      t.timestamps
    end
  end
end
