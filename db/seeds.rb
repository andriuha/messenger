User.delete_all
admin = User.create(email: 'admin@example.com', password: 'password', admin: true, name: 'Administrator')
alex = User.create(email: 'alex@example.com', password: 'password', name: 'Alex')
bob = User.create(email: 'bob@example.com', password: 'password', name: 'Bob')

20.times { |i| User.create(email: "user#{i}@example.com", password: 'password') }

Message.delete_all
Message.create body: 'hi', from: admin, to: alex, readed: true
Message.create body: 'hi', to: admin, from: alex, readed: true
Message.create body: 'unread message', from: bob, to: admin, readed: false



