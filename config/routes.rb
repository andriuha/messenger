Rails.application.routes.draw do
  root to: 'front#index'

  devise_for :users

  get 'messanger/index'
  get 'messanger/:id', to: 'messanger#conversation', as: 'messenger'
  post 'create_message', to: 'messanger#create'

  scope '/admin' do
    resources :users
    resources :messages
  end
end
