json.extract! user, :id, :admin, :locked, :created_at, :updated_at
json.url user_url(user, format: :json)
