$(document).on 'click', '#refresh-messages', -> refresh_messages()

$(document).ready ->
  setInterval ->
    refresh_messages() if $('#message_to_id').length
  , 3 * 1000

refresh_messages = ->
  $.ajax '/messanger/' + $('#message_to_id').val() + '.js'