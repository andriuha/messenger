class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  after_initialize :init_attributes

  has_many :messages_from_me, class_name: 'Message', foreign_key: 'from_id'

  scope :not_locked_and_except_user_ids, -> (user_ids) { where(locked: false).where.not(id: user_ids) }

  def self.with_new_messages(user_id)
    not_locked_and_except_user_ids(user_id)
      .joins(:messages_from_me).merge(Message.unread_to(user_id))
  end

  def full_name
    name || email
  end

  private

  def init_attributes
    self.admin = false if admin.nil?
    self.locked = false if locked.nil?
  end
end
