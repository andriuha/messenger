class Message < ApplicationRecord
  belongs_to :from, class_name: 'User'
  belongs_to :to, class_name: 'User'

  after_initialize :init_attributes

  scope :between, -> (user_1_id, user_2_id) {
    where(from_id: [user_1_id, user_2_id], to_id: [user_1_id, user_2_id])
  }

  scope :unread_to, -> (to_id) {
    where(to_id: to_id, readed: false)
  }

  scope :by_from_and_to, ->(from_id, to_id) { where(from_id: from_id, to_id: to_id) }

  def self.mark_all_as_readed(from_id, to_id)
    by_from_and_to(from_id, to_id).update_all(readed: true)
  end

  private

  def init_attributes
    self.readed = false if readed.nil?
  end
end
