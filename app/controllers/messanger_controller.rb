class MessangerController < ApplicationController
  respond_to :html, :js

  def index
    @users_with_new_messages = User.with_new_messages(current_user.id).distinct
    @users = User.not_locked_and_except_user_ids(@users_with_new_messages.map(&:id) << current_user.id)
  end

  def conversation
    Message.mark_all_as_readed(params[:id], current_user.id)
    @interlocutor = User.find(params[:id])
    @messages = Message.between(current_user.id, @interlocutor.id).order(created_at: :desc)
    @message = Message.new(from: current_user, to: @interlocutor)
  end

  def create
    @message = Message.create(message_params)
    redirect_to messenger_path(@message.to_id)
  end

  private

  def message_params
    params.require(:message).permit(:to_id, :body).merge(from_id: current_user.id)
  end
end
