require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  helper_method :user_is_admin?

  def user_is_admin?
    user_signed_in? && current_user.admin?
  end

  protected

  def configure_permitted_parameters
    user_additional_params = [:name, :sex, :birthday]
    devise_parameter_sanitizer.permit(:sign_up, keys: user_additional_params)
    devise_parameter_sanitizer.permit(:account_update, keys: user_additional_params)
  end
end
