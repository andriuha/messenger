class AdminController < ApplicationController
  before_action :check_administrator

  private

  def check_administrator
    unless user_is_admin?
      redirect_to root_path, alert: 'You are not administrator'
    end
  end
end